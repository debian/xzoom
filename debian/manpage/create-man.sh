#!/bin/bash

# by Eriberto
# Create the manpage using txt2man command.
#
# This script can be used under BSD-3-Clause license.

T2M_DATE="17 Jun 2020"
T2M_NAME=xzoom
T2M_VERSION=0.3
T2M_LEVEL=1
T2M_DESC="magnify part of X display, with real-time updates"

# Don't change the following line
txt2man -d "$T2M_DATE" -t $T2M_NAME -r $T2M_NAME-$T2M_VERSION -s $T2M_LEVEL -v "$T2M_DESC" $T2M_NAME.txt > $T2M_NAME.$T2M_LEVEL
